local ADDON_NAME, BTML = ...;
local L = BTML.L;

-- Make sure to hold the roll pattern to fetch rolls.
BTML.rollPattern = L["(.+) rolls (%d+) %((%d+)%-(%d+)%)"];

-- Whether the addon is fully loaded.
BTML.loaded = false;

-- Default addOn preferences:
BTML.defaultPrefs = {
	-- Enable/disable debug mode:
	debugMode = false,

	-- --------------------------------------------------------
	-- Default window settings:
	-- --------------------------------------------------------
	enforceUnique        = true,	-- Unique rolls per player.
	enforceMax           = true,	-- Rolls max is 100
	ignoreFixed          = true,	-- Ignore fixed rolls (ex: /roll 200)
	ignoreCountdown      = true,	-- Ignore rolls after countdown.
	useRaidWarning       = true,	-- Use raid warnings to announce.
	announceWinner       = true,	-- Announce the item winner.
	announceHolder       = true,	-- Announce items held for later rolling.
	announceBanker       = false,	-- Announce reserved items.
	announceDisenchanter = false,	-- Announce items to be disenchanted.

	-- --------------------------------------------------------
	-- Default frame position:
	-- --------------------------------------------------------
	frameRef = "CENTER",
	frameX   = 0,
	frameY   = 0,
	hidden   = false,
};

-- ----------------------------------------------------------------------
-- Event Handlers
-- ----------------------------------------------------------------------

-- Called when the addOn is ready.
function BTML.OnReady()
	-- Setting up default options:
	_G.BTMLPrefs = _G.BTMLPrefs or {};
	for k, v in pairs(BTML.defaultPrefs) do
		if not _G.BTMLPrefs[k] then
			_G.BTMLPrefs[k] = v;
		end
	end
	-- Create out UIFrame
	BTML.CreateUIFrame();
end

-- Called before logging out:
function BTML.OnSaving()
	if BTML.UIFrame then
		local point, relativeTo, relativePoint, xOfs, yOfs = BTML.UIFrame:GetPoint();
		_G.BTMLPrefs.frameRef = relativePoint;
		_G.BTMLPrefs.frameX   = xOfs;
		_G.BTMLPrefs.frameY   = yOfs;
	end
end

-- Called upon any update:
function BTML.OnUpdate()
	if not BTML.loaded or BTMLPrefs.hidden then
		return;
	end
	BTML.UpdateUIFrame();
end

-- Main events handler function:
function BTML.OnEvent(frame, event, ...)
	-- When the addon is loaded.
	if event == "ADDON_LOADED" then
		local name = ...;
		if name == ADDON_NAME then
			BTML.OnReady();
			BTML.Debug("BTMasterLooter Loaded!");
		end
		return;
	end
	-- Upon player's login.
	if event == "PLAYER_LOGIN" then
		BTML.loaded = true;
		return;
	end
	-- Upon player's logout.
	if event == "PLAYER_LOGOUT" then
		BTML.OnSaving();
		return;
	end
end

-- ----------------------------------------------------------------------
-- UIFrame Related Functions.
-- ----------------------------------------------------------------------

-- Creating the UIFrame
function BTML.CreateUIFrame()

	-- create the UI frame
	BTML.UIFrame = CreateFrame("Frame", "BTMLMain", UIParent);
	BTML.UIFrame:SetFrameStrata("DIALOG");
	BTML.UIFrame:SetWidth(232);
	BTML.UIFrame:SetHeight(435);

	-- make it black
	BTML.UIFrame:SetBackdrop({
		bgFile = "Interface\\DialogFrame\\UI-DialogBox-Background",
		edgeFile = "Interface\\DialogFrame\\UI-DialogBox-Border",
		tile = true,
		tileSize = 8,
		edgeSize = 8,
		insets = {top = 3, right = 3, bottom = 3, left = 3},
	});

	-- position it
	BTML.UIFrame:SetPoint(_G.BTMLPrefs.frameRef, _G.BTMLPrefs.frameX, _G.BTMLPrefs.frameY);

	-- make it draggable
	BTML.UIFrame:SetMovable(true);
	BTML.UIFrame:EnableMouse(true);

	-- --------------------------------------------- --
	-- CREATE FRAME BUTTONS
	-- --------------------------------------------- --

	-- "Select Item" button:
	local btnSelectItem = CreateFrame("Button", "BTMLMain_SelectItemBtn", BTML.UIFrame, "UIPanelButtonTemplate");
	btnSelectItem:SetPoint("TOPLEFT", BTML.UIFrame, "TOPLEFT", 5, -5);
	btnSelectItem = BTML.SetButtonPrefs(btnSelectItem, 110, 32, L["Select Item"]);
	btnSelectItem:SetScript("OnClick", function(self) BTML.ClickSelectItem(); end);

	-- "Spam Loot" button:
	local btnSpamLoot = CreateFrame("Button", "BTMLMain_SpamLootBtn", BTML.UIFrame, "UIPanelButtonTemplate");
	btnSpamLoot:SetPoint("LEFT", btnSelectItem, "RIGHT", 0, 0);
	btnSpamLoot = BTML.SetButtonPrefs(btnSpamLoot, 110, 32, L["Spam Loot"]);
	btnSpamLoot:SetScript("OnClick", function(self) BTML.ClickSpamLoot(); end);

	-- "MS" button (Main Spec):
	local btnMainSpecRoll = CreateFrame("Button", "BTMLMain_MainSpecRollBtn", BTML.UIFrame, "UIPanelButtonTemplate");
	btnMainSpecRoll:SetPoint("TOPLEFT", btnSelectItem, "BOTTOMLEFT", 5, -50);
	btnMainSpecRoll = BTML.SetButtonPrefs(btnMainSpecRoll, 32, 32, L["MS"]);
	btnMainSpecRoll:SetScript("OnClick", function(self) BTML.ClickMainSpecRoll(); end);

	-- "OS" button (Off Spec):
	local btnOffSpecRoll = CreateFrame("Button", "BTMLMain_OffSpecRollBtn", BTML.UIFrame, "UIPanelButtonTemplate");
	btnOffSpecRoll:SetPoint("LEFT", btnMainSpecRoll, "RIGHT", 3, 0);
	btnOffSpecRoll = BTML.SetButtonPrefs(btnOffSpecRoll, 32, 32, L["OS"]);
	btnOffSpecRoll:SetScript("OnClick", function(self) BTML.ClickOffSpecRoll(); end);

	-- "FREE" button (Free Rolls):
	local btnFreeRoll = CreateFrame("Button", "BTMLMain_FreeRollBtn", BTML.UIFrame, "UIPanelButtonTemplate");
	btnFreeRoll:SetPoint("LEFT", btnOffSpecRoll, "RIGHT", 3, 0);
	btnFreeRoll = BTML.SetButtonPrefs(btnFreeRoll, 48, 32, L["FREE"]);
	btnFreeRoll:SetScript("OnClick", function(self) BTML.ClickFreeRoll(); end);

	-- "Countdown" button:
	local btnCountdown = CreateFrame("Button", "BTMLMain_CountdownBtn", BTML.UIFrame, "UIPanelButtonTemplate");
	btnCountdown:SetPoint("LEFT", btnFreeRoll, "RIGHT", 6, 0);
	btnCountdown = BTML.SetButtonPrefs(btnCountdown, 90, 32, L["Countdown"]);
	btnCountdown:SetScript("OnClick", function(self) BTML.ClickCountdown(); end);

	-- We make sure it's hidden:
	-- BTML.UIFrame:Hide();
end

function BTML.SetButtonPrefs(btnFrame, btnWidth, btnHeight, btnText)
	if btnFrame ~= nil then
		btnFrame:SetWidth(btnWidth);
		btnFrame:SetHeight(btnHeight);
		btnFrame:SetText(btnText);
	end
	return btnFrame;
end

-- Setting elements' font size
function BTML.SetFontSize(str, size)
	local Font, Height, Flags = str:GetFont()
	if (not (Height == size)) then
		str:SetFont(Font, size, Flags)
	end
end

-- When the player starts moving the frame.
function BTML.OnDragStart(frame)
	BTML.UIFrame:StartMoving();
	BTML.UIFrame.isMoving = true;
	GameTooltip:Hide()
end

-- When the player  stops dragging
function BTML.OnDragStop(frame)
	BTML.UIFrame:StopMovingOrSizing();
	BTML.UIFrame.isMoving = false;
end

-- Update the UI Frame.
function BTML.UpdateUIFrame()
	-- Things to do
end

-- ----------------------------------------------------------------------
-- Slash Commands
-- ----------------------------------------------------------------------
SLASH_BTM1 = "/btm";
SLASH_BTM2 = "/btml";
SlashCmdList["BTM"] = function(msg, editBox)
    local command, rest = msg:match("^(%S*)%s*(.-)$");    
    BTML.Debug("Command = " .. command or "")
	if (command == "" or command == "toggle" or command == L["toggle"]) then
		if BTML.UIFrame:IsShown() then BTML.UIFrame:Hide();
		else BTML.UIFrame:Show(); end
	elseif (command == "show" or command == L["show"]) then
		BTML.UIFrame:Show();
	elseif (command == "hide" or command == L["hide"]) then
		BTML.UIFrame:Hide();
	elseif (command == "log") then
		-- BTML_Logger:SlashCmdHandler(rest, editBox)
	else
		BTML.Print("Help lines");
		-- BTML.Print(BTM_CORE_HELP_1..BTM_CORE_HELP_2..BTM_CORE_HELP_3..BTM_CORE_HELP_4..BTM_CORE_HELP_5)
	end
end

-- ----------------------------------------------------------------------
-- Chat Output Functions
-- ----------------------------------------------------------------------

-- Simple print to chat window
function BTML.Print(str)
	if str then
		DEFAULT_CHAT_FRAME:AddMessage(str);
	end
end

-- Debug function.
function BTML.Debug(str)
	if str and BTMLPrefs.debugMode then
		DEFAULT_CHAT_FRAME:AddMessage("|cff15ff15"..str.."|r");
	end
end

-- Print Error Message:
function BTML.Error(str)
	if msg then
		UIErrorsFrame:AddMessage(msg, 1.0, 1.0, 0.5, 53, 3);
	end
end

-- Print Info Message:
function BTML.Info(str)
	if msg then
		UIErrorsFrame:AddMessage(msg, 1.0, 1.0, 0.5, 53, 3);
	end
end

-- Function used for different party/raid announcements:
function BTML.Announce(str)
	if str == nil then
		return;
	end
	local chatType = "SAY";
	if BTML.IsInParty() then chatType = "PARTY"; end
	if BTML.IsInRaid() then
		chatType = "RAID";
		-- Using raid Warnings?
		if BTMLPrefs.useRaidWarning then
			chatType = "RAID_WARNING";
		end
	end
	SendChatMessage(str, chatType);
end

-- ----------------------------------------------------------------------
-- Helpers Functions
-- ----------------------------------------------------------------------

-- Checks if the player is in a party
function BTML.IsInParty()
	return GetNumPartyMembers() ~= 0;
end

-- Checks if the player is in a raid group
function BTML.IsInRaid()
	return GetNumRaidMembers() ~= 0;
end

-- Checks if the player is the master looter:
function BTML.IsMasterLooter()
	local lootMethod, masterLooterPartyID, masterLooterRaidID = GetLootMethod();
	return (masterLooterPartyID and masterLooterPartyID == 0);
end

-- Returns class color by its name
function BTML.GetClassColor(className)
	if className == "DEATH KNIGHT" then
		className = "DEATHKNIGHT";
	end

	local colors = RAID_CLASS_COLORS
	if colors[className] == nil then
		BTML.Debug("No such class: "..className);
		return 0, 0, 0;
	end

	return colors[className].r, colors[className].g, colors[className].b;
end

-- ----------------------------------------------------------------------
-- UI Buttons Clicked
-- ----------------------------------------------------------------------

function BTML.ClickSelectItem()
	BTML.Announce("Selecting a new item");
end

function BTML.ClickSpamLoot()
	BTML.Announce("The boss dropped: ");
end

function BTML.ClickMainSpecRoll()
	BTML.Announce("Roll for MS on: ");
end

function BTML.ClickOffSpecRoll()
	BTML.Announce("Roll for OS on: ");
end

function BTML.ClickFreeRoll()
	BTML.Announce("Roll on: ");
end

function BTML.ClickCountdown()
	BTML.Announce("Rolling ends in: 5 seconds");
end

-- ----------------------------------------------------------------------
-- Initialize Out AddOn
-- ----------------------------------------------------------------------
BTML.EventFrame = CreateFrame("Frame");
-- BTML.EventFrame:Show();
BTML.EventFrame:SetScript("OnEvent", BTML.OnEvent);
BTML.EventFrame:SetScript("OnUpdate", BTML.OnUpdate);
BTML.EventFrame:RegisterEvent("ADDON_LOADED");
BTML.EventFrame:RegisterEvent("PLAYER_LOGIN");
BTML.EventFrame:RegisterEvent("PLAYER_LOGOUT");