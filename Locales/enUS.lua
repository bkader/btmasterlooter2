local _, BTML = ...;
local L = setmetatable({}, {__index = function(t, k)
	local v = tostring(k);
	rawset(t, k, v);
	return v;
end});

-- Roll Pattern: Make sure to localize this line depending on your locale:
-- L["(.+) rolls (%d+) %((%d+)%-(%d+)%)"] = ""
BTML.L = L;