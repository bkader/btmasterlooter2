## Interface: 30300
## Title : BTMasterLooter
## Notes: Blame The Master Looter, making master looter easier.
## Author: Kader B (Earwin on Warmane-Icecrown)
## LoadOnDemand: 0
## DefaultState: enabled
## SavedVariablesPerCharacter: BTMLPrefs
Locales\enUS.lua
Locales\frFR.lua
BTMasterLooter.lua